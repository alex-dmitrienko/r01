setwd("/Users/alex/Dropbox/Research/Projects/docker/r01/myapp")
source("supportive.R")

library(MASS)

# Trial 022

# One-sided alpha
alpha = 0.025

# Number of families of null hypotheses (number of endpoints)
nfams = 2

# Number of doses
ndoses = 2

# Endpoint 1
plac1 = 25
low1 = 50
high1 = 55

# Endpoint 2
plac2 = 10
low2 = 22
high2 = 30

# No active control

sample_size_per_arm = 110

# Number of simulations
n.sims = 10000


###################################### 

# arm_labels = c("Placebo", "OKZ Low", "OKZ High", "Active control")

arm_labels = c("Endpoint", "Placebo", "OKZ Low", "OKZ High")

endpoint_labels = c("ACR20 at Week 14", 
                    "DAS28 [CPR]<3.2 at Week 14") 

theta = rep(0, ndoses * nfams)

# Endpoint 1 (Primary endpoint): 
# Low dose 
p11 = plac1 / 100
p12 = low1 / 100
pave = (p11 + p12)/2
theta[1] = abs(p11 - p12)/sqrt(pave*(1-pave))

# High dose 
p11 = plac1 / 100
p13 = high1 / 100
pave = (p11 + p13)/2
theta[2] = abs(p11 - p13)/sqrt(pave*(1-pave))

# Endpoint 2 (secondary endpoint): 
# Low dose  
p21 = plac2 / 100
p22 = low2 / 100
pave = (p21 + p22)/2
theta[3] = abs(p21 - p22)/sqrt(pave*(1-pave))

# High dose 
p21 = plac2 / 100
p23 = high2 / 100
pave = (p21 + p23)/2
theta[4] = abs(p21 - p23)/sqrt(pave*(1-pave))

assumptions = matrix(0, nfams, 4)
assumptions = as.data.frame(assumptions)

assumptions[1, 1] = endpoint_labels[1]
assumptions[1, 2] = as.character(100 * p11); assumptions[1, 3] = as.character(100 * p12); 
assumptions[1, 4] = as.character(100 * p13); 

assumptions[2, 1] = endpoint_labels[2]
assumptions[2, 2] = as.character(100 * p21); assumptions[2, 3] = as.character(100 * p22); 
assumptions[2, 4] = as.character(100 * p23); 

colnames(assumptions) = arm_labels

#############################################################


results022 = Trial022(nfams, ndoses, theta, sample_size_per_arm, n.sims)

results022

library(googleVis)

# Assumptions
plot(gvisTable(assumptions))

row_labels = c(paste0(endpoint_labels[1], " (Low dose vs placebo)"),
               paste0(endpoint_labels[1], " (High dose vs placebo)"),
               paste0(endpoint_labels[2], " (Low dose vs placebo)"),
               paste0(endpoint_labels[2], " (High dose vs placebo)"))

marginal = data.frame(row_labels, results022[[1]]$correlation_scenario[[1]][[1]], results022[[1]]$correlation_scenario[[2]][[1]], results022[[1]]$correlation_scenario[[3]][[1]])

colnames(marginal) = c("Test", "Correlation = 0.2", "Correlation = 0.4", "Correlation = 0.6")

plot(gvisTable(marginal))

row_labels = endpoint_labels

disjunctive = data.frame(row_labels, results022[[1]]$correlation_scenario[[1]][[2]], results022[[1]]$correlation_scenario[[2]][[2]], results022[[1]]$correlation_scenario[[3]][[2]])

colnames(disjunctive) = c("Family of tests", "Correlation = 0.2", "Correlation = 0.4", "Correlation = 0.6")

plot(gvisTable(disjunctive))



